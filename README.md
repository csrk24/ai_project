![Hier sollte ein Titelbild zu sehen sein.](title.png)
## Deutsch
_Dieses Projekt ist von der Aufgabenstellung her in deutscher Sprache umgesetzt, eine englische Version kann allerdings jeweils am Ende des Dokumentes gefunden werden. Der ursprüngliche Aufgabentext wurde dabei mit [google translator](https://translate.google.de/), übersetzt. Die Gesamtdokumentation als pdf ist nur auf Deutsch vorhanden und kann im `documentation`-Branch gefunden werden._

_Die unterschiedlichen Lösungen & Implementierung kann im jeweiligen Branch gefunden werden (Task 1-6, Task = (engl.) Aufgabe)._

## Projekt des Moduls "Grundlagen der künstlichen Intelligenz" im WiSe21/22.

Projekt 1: Chatbots

_von Konstantin Seen & Christian Struck_
## Aufgabe:
In diesem Projekt sollen Sie einen intelligenten Chatbot entwerfen. Hierfür
sollen Sie das Framework RASA und die Python Bibliothek spaCy verwen-
den. Mit der Bibliothek spaCy soll ein Modell trainiert werden, welches die
Bedeutung (Intent) eines Textes klassifiziert. Basierend auf den jeweiligen
Intents soll mit Rasa ein Chatbot erstellt werden. Zum Beispiel sollen ver-
schiedene Begrüßungsformen als Intent greeting erkannt werden. Sobald ein
Intent erkannt wird, soll der Bot eine entsprechende Antwort liefern. Die
Antworten werden via RASA definiert. Neben einem Intent kann ein Text
auch sogenannte Entities beinhalten. Im Satz "Ich studiere Informatik an der THM“ ist zum Beispiel "THM“ eine entity. Solche Entitäten müssen oft erkannt und klassifiziert werden, um Texte zu verstehen.
Unter [rasa](https://rasa.com/) und [spacy](https://spacy.io/) finden Sie die entsprechende Bibliothek. Zu beiden gibt es ausführliche Dokumentationen.

### Aufgabe 1 (Installation). 
Installieren Sie die spaCy Bibliothek und bringen Sie diese zum Laufen.

### Aufgabe 2 (Intent Classification mit spaCy). 
Ziel dieser Aufgabe ist es, sich mit der spaCy Bibliothek vertraut zu machen. Trainieren Sie ein einfaches Modell, welches den Intent eines Textes identifiziert. Hierfür müssen
Sie einen eigenen Datensatz erstellen. Der Datensatz sollte zwei Intents enthalten, Begrüßung und Abschied. Trainieren Sie nun ein Modell welches für
neue Texte den Intent bestimmt. Testen Sie Ihr Modell mit beliebigen kurzen
Texten.

### Aufgabe 3 (Entity Extraction mit spaCy). 
Ähnlich wie in der vorherigen Aufgabe sollen Sie nun eine Entity Bestimmung (Entity Extraction)
mit spaCy durchführen. Erstellen Sie hier Beispieltext und versuchen mit
vortrainierten Modellen von spaCy die Entitäten zu extrahieren.

### Aufgabe 4 (Ein erster Chatbot mit RASA). 
Im folgenden sollen Sie mit RASA einen ersten Chatbot erstellen, welcher spaCy als Backend verwendet, um Intent Klassifikation und Entity Bestimmung durchzuführen. Unter
[Rasa-Playground](https://rasa.com/docs/rasa/playground) finden Sie einige erste Beispieldaten
und einen Ablauf zur Erstellung eines Chatbots mit RASA. Die dort gezeigten Schritte sollen Sie selber mit Hilfe von Python implementieren.

### Aufgabe 5 (Support-Chatbot). 
Erstellen Sie einen intelligenten Chatbot
mit Hilfe der genannten Technologien. Das Themengebiet des Chatbots ist
Ihnen überlassen. RASA erlaubt es beliebige Actions auszuführen bevor eine
Antwort an den User zurückgegeben wird, diese sollen genutzt werden. Zum
Beispiel könnte der Chatbot automatisch Einträge in einen Kalender vornehmen, Google-Suchen durchführen, ein FAQ realisieren oder Tic-Tac-Toe
spielen. Sie können sich auch auf ein spezielles Themengebiet konzentrieren,
zum Beispiel Restaurants vorschlagen und Reservierungen vornehmen.

### Aufgabe 6 (Bot für Discord oder IRC). 
Discord und IRC sind weitverbreitete Chatprogramme. Discord bietet seinen Entwicklern eine einfache [API](https://discordapp.com/developers/docs/intro), um Bots zu erstellen, die
Nachrichten lesen und verschicken können. IRC bietet keine direkte API, da
es sich nur um ein Protokoll handelt, es ist jedoch ebenso möglich mit [Python](https://linuxacademy.com/blog/linux-academy/creating-an-irc-bot-with-python3/)
einen Chatbot anzubinden. Verbinden Sie Ihren Chatbot mir Discord oder
IRC, um einfach mit Ihrem Bot zu chatten.


## English
_Project originally in german, tasks, etc. translated with [google translator](https://translate.google.de/), german version can be found at the end of each file._

_The different Task implementations and solutions can be found in the related branches._

_The documentation (german) can be found in the `documentation` branch._

## Project of the "Introduction to Artificial Intelligence" module in WiSe21/22.

Project 1: Chatbots

_by Konstantin Seen & Christian Struck_
## Task:

In this project you should design an intelligent chatbot. Therefor
should you use the RASA framework and the spaCy Python library
the. A model is to be trained with the spaCy library, which the
Classifies meaning (intent) of a text. Based on the respective
Intents a chatbot should be created with Rasa. For example,
different forms of greeting can be recognized as intent greeting. Once an
intend is detected, the bot should provide an appropriate answer. The
Answers are defined via RASA. In addition to an intent, a text
can also contain so-called entities. In the sentence "I study computer science at the THM", for example, "THM" is an entity. Such entities often have to be recognized and classified in order to understand texts.
The corresponding library can be found under [rasa](https://rasa.com/) and [spacy](https://spacy.io/). There is extensive documentation for both.

### Task 1 (installation).
Install the spaCy library and make it work.

### Task 2 (Intent Classification with spaCy).
The aim of this task is to familiarize yourself with the spaCy library. Train a simple model that identifies the intent of a text. For this you have to
You create your own data set. The data set should contain two intents, greeting and farewell. Now train a model which for
new texts determine the intent. Test your model with any short one
Texts.

### Task 3 (Entity Extraction with spaCy).
Similar to the previous task, you should now determine an entity (Entity Extraction)
perform with spaCy. Create sample text here and try with
to extract the entities from pre-trained models from spaCy.

### Task 4 (A first chatbot with RASA).
In the following you should create a first chatbot with RASA, which spaCy uses as a backend to carry out intent classification and entity determination. Under
[Rasa-Playground](https://rasa.com/docs/rasa/playground) you will find some initial sample data
and a process for creating a chatbot with RASA. You should implement the steps shown there yourself using Python.

### Task 5 (support chatbot).
Create a smart chatbot
with the help of the technologies mentioned. The topic of the chatbot is
Up to you. RASA allows any actions to be carried out before a
Response is returned to the user, these should be used. To the
For example, the chatbot could automatically make entries in a calendar, carry out Google searches, implement an FAQ or tic-tac-toe
play. You can also concentrate on a specific topic,
for example suggesting restaurants and making reservations.

### Task 6 (bot for Discord or IRC).
Discord and IRC are widely used chat programs. Discord offers its developers a simple [API](https://discordapp.com/developers/docs/intro) to create bots that
Read and send messages. IRC doesn't provide a direct API, there
it is just a protocol, but it is also possible with [Python](https://linuxacademy.com/blog/linux-academy/creating-an-irc-bot-with-python3/)
to connect a chatbot. Connect your chatbot to Discord or
IRC to easily chat with your bot.




